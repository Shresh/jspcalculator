package com.iims.midterm.jspcalculator;

import com.iims.midterm.jspcalculator.evaluation.Evaluation;
import com.iims.midterm.jspcalculator.model.Calculator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "calculatorServlet", value = "/calculatorMain")
public class CalculatorServlet extends HttpServlet {

    public void init(){}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        PrintWriter out = response.getWriter();

        Calculator calc = new Calculator();
        calc.setInvalue(request.getParameter("inputValue"));


        try{
            if(calc.getInvalue() == null){
                request.setAttribute("error", "Paranthesis does not match");
            }else if(Evaluation.checkParenthesis(calc.getInvalue()) == true){
                calc.setPostfixExpression(Evaluation.InfixToPostfix(calc.getInvalue()));
                calc.setResult(Evaluation.Calculation(calc.getPostfixExpression()));
                request.setAttribute("result", Integer.toString(calc.getResult()));
                System.out.println(calc.getResult());
            }
        }catch(Exception e){
                request.setAttribute("error", e.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
}
