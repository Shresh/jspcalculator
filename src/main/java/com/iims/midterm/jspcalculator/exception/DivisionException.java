package com.iims.midterm.jspcalculator.exception;

import javax.swing.*;

public class DivisionException extends Exception {
    public DivisionException() throws Exception {
        throw new Exception("[Division Error] Cannot divide a number by 0");
    }
}
