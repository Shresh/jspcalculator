package com.iims.midterm.jspcalculator.evaluation;


import java.util.Stack;

public class Evaluation {

    public static String InfixToPostfix(String infixExpression) {

        boolean numberEnd = false;
        String postfixExpression = "";
        Stack stack = new Stack();

        for (int i = 0; i < infixExpression.length(); i++) {
            char infixExpChar = infixExpression.charAt(i);
            switch (infixExpChar) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    postfixExpression = postfixExpression.concat(infixExpChar + "");
                    numberEnd = true;
                    break;
                case '(':
                    if (numberEnd == true) {
                        postfixExpression = postfixExpression.concat(" ");
                        numberEnd = false;
                    }
                    break;
                case ')':
                    if (numberEnd == true) {
                        postfixExpression = postfixExpression.concat(" ");
                        numberEnd = false;
                    } else {
                        while (((Character) stack.peek()).charValue() != '(') {
                            postfixExpression = postfixExpression.concat(((Character) stack.pop()).toString());
                        }
                    }
                    Object openParenthesis = stack.pop();
                    break;
                case '+':
                case '-':
                case '*':
                case '/':
                    if (numberEnd == true) {
                        postfixExpression = postfixExpression.concat(" ");
                        numberEnd = false;
                    }
                    while (!stack.isEmpty() && (((Character) stack.peek()).charValue()) != '(' && (Precedence(infixExpChar)) <= Precedence(((Character) stack.peek()).charValue())) {
                        postfixExpression = postfixExpression.concat(((Character) stack.pop()).toString());
                    }
                    stack.push(new Character(infixExpChar));
                    break;
            }
        }
        if (numberEnd == true) {
            postfixExpression = postfixExpression.concat(" ");
            numberEnd = false;
        }
        while (!stack.isEmpty()) {
            postfixExpression = postfixExpression.concat(((Character) stack.pop()).toString());
        }
//        for checking the postfix information for study purpose an intenteional log.
        System.out.println(postfixExpression);
        return postfixExpression;
    }

    public static int Calculation(String postfixExpression) throws Exception {

        int value1, value2;
        String temporary = "";
        Stack stack = new Stack();

        for (int i = 0; i < postfixExpression.length(); i++) {
            char postfixExpChar = postfixExpression.charAt(i);
            switch (postfixExpChar) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    temporary = temporary.concat(postfixExpChar + "");
                    break;
                case ' ':
                    stack.push(new Integer(temporary));
                    temporary = new String();
                    break;
                case '+':
                    value1 = new Integer(((Integer) stack.pop()).intValue() + ((Integer) stack.pop()).intValue());
                    stack.push(value1);
                    break;
                case '-':
                    value2 = new Integer(((Integer) stack.pop()).intValue());
                    value1 = new Integer(((Integer) stack.pop()).intValue() - value2);
                    stack.push(value1);
                    break;
                case '*':
                    value1 = new Integer(((Integer) stack.pop()).intValue() * ((Integer) stack.pop()).intValue());
                    stack.push(value1);
                    break;
                case '/':
                    value2 = new Integer(((Integer) stack.pop()).intValue());
                    value1 = new Integer(((Integer) stack.pop()).intValue() / value2);
                    if (value2 == 0) {
                        throw new Exception("[Parenthesis Error] Parenthesis does not match");
                    }
                    stack.push(value1);
                    break;
            }
        }
        return (int) stack.peek();
    }

    public static int Precedence(char operator) {
        int precedence = 0;
        switch (operator) {
            case '+':
            case '-':
                precedence = 1;
                break;
            case '*':
            case '/':
                precedence = 2;
                break;
        }
        return precedence;
    }

    public static boolean checkParenthesis(String exp) throws Exception {
        Stack stack = new Stack();
        for (int i = 0; i < exp.length(); i++) {
            char character = exp.charAt(i);
            if (character == '(') {
                stack.push(new Character(character));
            } else if (character == ')') {
                if (stack.isEmpty()) {
                    throw new Exception("[Parenthesis Error] Parenthesis does not match");
                } else {
                    return true;
                }
            }
        }
        return stack.isEmpty();
    }
}
