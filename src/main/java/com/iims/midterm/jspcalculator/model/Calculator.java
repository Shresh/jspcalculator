package com.iims.midterm.jspcalculator.model;

public class Calculator {
private String outcome;
private String invalue;
private String postfixExpression;
private int result;

    public Calculator(String outcome, String invalue, String postfixExpression, int result) {
        this.outcome = outcome;
        this.invalue = invalue;
        this.postfixExpression = postfixExpression;
        this.result = result;
    }

    public Calculator() {
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getInvalue() {
        return invalue;
    }

    public void setInvalue(String invalue) {
        this.invalue = invalue;
    }

    public String getPostfixExpression() {
        return postfixExpression;
    }

    public void setPostfixExpression(String postfixExpression) {
        this.postfixExpression = postfixExpression;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
