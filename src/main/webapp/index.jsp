<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP Calculator</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./assets/css/style.css">
    <script src="./assets/js/index.js"></script>
</head>
<body>
<div class="container my-4">
    <% String error =  request.getParameter("error");
        if(error != null) {
            %>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <img src="..." class="rounded mr-2" alt="...">
            <strong class="mr-auto">Bootstrap</strong>
            <small class="text-muted">0 sec ago</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
           ${error}
        </div>
    </div>
    <%
        }
    %>
    <hr class="mb-5"/>
<div class="calculator card">



    <form action="calculatorMain" method="post">

        <input type="text" name="inputValue" class="calculator-screen z-depth-1" id="screen" value="${result}" readonly="readonly"/>

        <div class="calculator-keys">

            <button type="button" class="operator btn btn-info" onclick="updateScreen('+')" value="+">+</button>
            <button type="button" class="operator btn btn-info" onclick="updateScreen('-')" value="-">-</button>
            <button type="button" class="operator btn btn-info" onclick="updateScreen('*')" value="*">&times;</button>
            <button type="button" class="operator btn btn-info" onclick="updateScreen('/')" value="/">&divide;</button>

            <button type="button" value="7" class="btn btn-light waves-effect" onclick="updateScreen('7')">7</button>
            <button type="button" value="8" class="btn btn-light waves-effect" onclick="updateScreen('8')">8</button>
            <button type="button" value="9" class="btn btn-light waves-effect" onclick="updateScreen('9')">9</button>


            <button type="button" value="4" class="btn btn-light waves-effect" onclick="updateScreen('4')">4</button>
            <button type="button" value="5" class="btn btn-light waves-effect" onclick="updateScreen('5')">5</button>
            <button type="button" value="6" class="btn btn-light waves-effect" onclick="updateScreen('6')">6</button>


            <button type="button" value="1" class="btn btn-light waves-effect" onclick="updateScreen('1')">1</button>
            <button type="button" value="2" class="btn btn-light waves-effect" onclick="updateScreen('2')">2</button>
            <button type="button" value="3" class="btn btn-light waves-effect" onclick="updateScreen('3')">3</button>


            <button type="button" value="0" class="btn btn-light waves-effect" onclick="updateScreen('0')">0</button>
            <button type="button" class="decimal function btn btn-secondary" value="." onclick="updateScreen('.')">.</button>
            <button type="button" class="all-clear function btn btn-danger btn-sm" value="all-clear" onclick="clearScreen()">AC</button>

            <button type="submit" class="equal-sign operator btn btn-info" value="=">=</button>

        </div>
    </form>
</div>
</div>
</body>
</html>